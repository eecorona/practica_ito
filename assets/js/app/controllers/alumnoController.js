ng.controller('alumnoController', ['$scope', '$http', '$routeParams', '$location', 'Upload', '$timeout',
  function($scope, $http, $routeParams, $location, Upload, $timeout) {

    /*
  1.- ir por el alumno que me indica $routeParams.id
*/

    $scope.$on('$viewContentLoaded', () => {
      $scope.r = Math.random();
      $scope.alumno = {};
      if($routeParams.id!=='nuevo'){
        $http.post('/alumnos/uno',{id:$routeParams.id}).then(
        function success(response){
          console.log('respuesta de obtener alumno:', response);
          if(response.data && response.data.id){
            $scope.alumno = response.data;


            $timeout(() => {
              new QRCode('elqrcode', {
                text: $scope.alumno.id,
                width: 100,
                height: 100,
                colorDark : '#000000',
                colorLight : '#ffffff',
                correctLevel : QRCode.CorrectLevel.H
              });
            }, 100);

          }
        },
        function error(error){
          alertify.error('Se produjo un error al obtener el alumno.');
          console.log('error al obtener alumno:', error);
        }
        );
      }
    });

    $scope.guardar= function(){
      $http.post('/alumnos/guardar',{alumno:$scope.alumno}).then(
      function success(response){
        console.log('Resultado de guardar:', response);
        var id;
        if(response.data.id){
          id=response.data.id;
        }else if(response.data.length && response.data[0].id){
          id=response.data[0].id;
        }

        $location.path('/alumno/'+id).replace();
        alertify.success('Edicion completa.');
      }, function error(err){
        alertify.error('No se pudo guardar.');
        console.log('Error al guardar:', err);
      }
      );
    };

    $scope.subiendoFoto = false;

    $scope.subirFoto = function(file){

      if (!file) {
        return;
      }

      $scope.subiendoFoto = true;
      $scope.porcentaje=0;
      var carga = {
        url: '/alumnos/subir-foto',
        data: {
          file: file,
          alumnoid: $scope.alumno.id
        }
      };

      Upload.upload(carga).then((resp) => {
        console.log('alumnoController->subirFoto->response: ', resp);
        $scope.subiendoFoto = false;
        if(resp.data.result){
          $scope.alumno.foto = true;
          $scope.r = Math.random();
        }else{
          alertify.error('No se pudo subir la imagen.');
        }
      }, (resp) => {
        console.log('alumnoController->subirFoto->error: ', resp);
        alertify.error('Error al subir la imagen.');
        $scope.subiendoFoto = false;
      }, (evt) => {
        $timeout(() => {
          $scope.porcentaje = parseInt(100.0 * evt.loaded / evt.total);
        });
      });
    };

  }]);
