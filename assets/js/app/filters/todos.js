ng.filter('timeago', () => {
  return function(valor){
    return moment(valor).fromNow();
  };
});


ng.filter('totalizar', () => {
  return function(coleccion, campo){
    if(!campo) {campo='total';}
    return _.sumBy(coleccion, (obj) => {
      return parseFloat(obj[campo]);
    });
  };
});
