ng.directive('direccion', ['$http',($http) => {
  return {
    restrict: 'E',
    scope: {
      objeto:'=',
      editable:'='
    },
    link: function(scope, el, attr){
    },
    templateUrl:'templates/directives/direccion.html'
  };
}]);
