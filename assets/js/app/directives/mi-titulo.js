ng.directive('miTitulo', [() => {
  return {
    transclude: true,
    restrict: 'E',
    scope: {
    },
    templateUrl:'templates/directives/mi-titulo.html'
  };
}]);
