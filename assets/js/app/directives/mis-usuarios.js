ng.directive('misUsuarios', ['$http',($http) => {
  return {
    restrict: 'E',
    scope: {
    },
    link: function(scope, el, attr){
      $http.get('/usuarios/get-all').then(
        function succcess(response){
          if(response.data.length){
            scope.usuarios = response.data;
          }
        },
        function error(err){
          console.log('Sucedió un error el obtener los usuarios', err);
        }
      );
    },
    templateUrl:'templates/directives/mis-usuarios.html'
  };
}]);
