ng.run(['$rootScope', '$location',function($rootScope, $location) {

  $rootScope.$on('$routeChangeStart', (event, next, current) => {
    console.log('INTENTO DE CAMBIO DE RUTA, Actual:', current, 'Siguiente:',next);

    //si es a una pagina abierta... continuar
    if (next && next.$$route && (next.$$route.originalPath === '/login' || next.$$route.originalPath === '/acercade' || next.$$route.originalPath === '/welcome')){
      return true;
    }

    //verificar que tenga sesion
    if(!$rootScope.main.UsuarioFactory.usuario.id){ //no deberia entrar a menos que sea a login
      console.log('Acceso denegado, redireccionando a login');
      event.preventDefault();
      $location.path('/login').replace();
      return false;
    }
  });

  $rootScope.$on('$routeChangeSuccess', (event, current) => {
    console.log('CAMBIO DE RUTA', current);
    if(current && current.$$route && current.$$route.originalPath) {
      $rootScope.main.rutaActual = current.$$route.originalPath;
    }
    //console.log("ruta actual:", $rootScope.main.rutaActual);
  });
}]);


ng.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  console.log('Etapa Config (ROUTES)');

  $locationProvider.hashPrefix(''); //quitar el ! de las rutas
  $routeProvider
    .when('/login', { //configuracion de rutas
      templateUrl: 'templates/controllers/loginController.html', //ruta del archivo
      controller: 'loginController', //nombre del controller
    })
    .when('/dashboard', {
      templateUrl: 'templates/controllers/dashboardController.html', //ruta del archivo
      controller: 'dashboardController', //nombre del controller
    })
    .when('/acercade', {
      templateUrl: 'templates/controllers/acercadeController.html', //ruta del archivo
      controller: 'acercadeController', //nombre del controller
    })
    .when('/alumnos', {
      templateUrl: 'templates/controllers/alumnosController.html', //ruta del archivo
      controller: 'alumnosController', //nombre del controller
    })
    .when('/alumno/:id', {
      templateUrl: 'templates/controllers/alumnoController.html', //ruta del archivo
      controller: 'alumnoController', //nombre del controller
    })
    .when('/welcome', {
      templateUrl: 'templates/controllers/welcomeController.html', //ruta del archivo
      controller: 'welcomeController', //nombre del controller
    })
    .otherwise({
      redirectTo: '/welcome'
    });

}]);
