
module.exports = function(req, res, next) {
  if (req.session.usuario.perfil!=='admin') {
    sails.log('No es perfil admin');
    return res.status(403).send({err: 'Debe traer perfil administrador'});
  }

  return next();

};
